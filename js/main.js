// Course Description Js
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

// Scroll JS Functions
$(window).scroll(function() {
  if ($(window).scrollTop() < 350) {
    $(".navbar").removeClass("fixed-top");
  } else {
    $(".navbar").addClass("fixed-top");
  }
});

setInterval(function() {
  $(".custom-social-proof")
    .stop()
    .slideToggle("slow");
}, 8000);
$(".custom-close").click(function() {
  $(".custom-social-proof")
    .stop()
    .slideToggle("slow");
});
